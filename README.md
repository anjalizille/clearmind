# Clearmind - Systematic Trading & Investment Strategies

Welcome to the trading website that offers systematic trading advice and investment strategies. This project is built to provide users with tools and insights to enhance their trading experience through objective, data-driven strategies and features.

## Features

- **Objective and Adaptive Investing**: A disciplined approach to investing that adapts to market conditions, aiming to minimize risk and maximize returns.
- **Trend Following 📈**: Follow market trends until they reverse, ensuring you're always in line with the market flow.
- **Backing Leaders 💪🏻**: Focus on investing in market leaders with strong momentum for better performance.
- **Balanced Concentration ⚖️**: Neither too diversified nor too concentrated, aiming for a balanced portfolio approach.
- **Smart Beta 🅱️**: Capture market inefficiencies to work in your favor with a tailored investment strategy.
- **System Driven 🤖**: Reduce human bias with systematic, rule-based trading approaches.

## Project Structure

```bash
├── public
│   ├── assets
│   │   └── investor-charter.pdf    # Investor Charter PDF for download
├── src
│   ├── components                  # Reusable UI components (cards, buttons, etc.)
│   ├── pages                       # Main website pages (Landing, About, etc.)
│   ├── styles                      # Styling (using Tailwind CSS)
│   ├── utils                       # Utility functions and helpers
├── README.md


## Installation

To get started with the project, follow these steps:

1. **Clone the repository**:

    ```bash
    git clone https://github.com/your-username/trading-website.git
    cd trading-website
    ```

2. **Install dependencies**:

    ```bash
    npm install
    ```

3. **Run the development server**:

    ```bash
    npm run dev
    ```

4. **Build the project for production**:

    ```bash
    npm run build
    ```

## Technologies Used

- **Next.js**: Framework for building server-side rendered React applications.
- **React**: JavaScript library for building user interfaces.
- **Tailwind CSS**: Utility-first CSS framework for styling.
- **Framer Motion**: Library for React animations.
- **FastAPI**: Python-based backend API for fetching and processing market data.

