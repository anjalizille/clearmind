import { Metadata } from "next";

export const SITE_CONFIG: Metadata = {
    title: {
        default: "Clearmind - Invest Better",
        template: `%s | Clearmind`
    },
    description: "Financial tools to help you make better investment decisions. Cycle app, Deep-I, backtesting services.",
    keywords: "Equity, investments, wealth, management, backtesting, finance, guidance",
    icons: {
        icon: [
            {
                url: "/icons/cm_logo.png",
                href: "/icons/cm_logo.png",
            }
        ]
    },
    openGraph: {
        title: "Invest Better | Clearmind | Equity Investments",
        description: "Financial tools to help you make better investment decisions. Cycle app, Deep-I, backtesting services.",
        siteName: "Clearmind",
        images: [
            {
                url: "/public/cm_logo.png",
                alt: "Clearmind",
            }
        ],
    },
    twitter: {
        card: "summary_large_image",
        title: "Invest Better | Clearmind | Equity Investments",
        description: "Financial tools to help you make better investment decisions. Cycle app, Deep-I, backtesting services.",
        site: "@iam_clearmind",
        images:["https://x.com/iam_clearmind/photo"]
    },
    metadataBase: new URL("https://iamclearmind.com"),
};


