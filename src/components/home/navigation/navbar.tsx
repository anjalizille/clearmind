import { Container, Icons } from "@/components";
import { Button, buttonVariants } from "@/components/ui/button";
import { Dialog, DialogClose } from "@/components/ui/dialog";
import {
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import { MenuIcon } from "lucide-react";

import Link from "next/link";

const Navbar = async () => {
  return (
    <header className="px-4 h-[60px] fixed top-0 left-0  w-full bg-background backdrop-blur-lg border-b border-border z-50">
      <Container reverse>
        <div className="flex items-center justify-between h-full mx-auto md:max-w-screen-xl">
          
        

          <div className="flex items-center justify-center space-x-1">
          <div className="flex justify-between w-full lg:hidden">
            <Dialog>
              <SheetTrigger className=" transition">
                <Button
                  size="icon"
                  variant="ghost"
                  className="w-8 h-8"
                  aria-label="Open menu"
                  asChild
                >
                  <MenuIcon className="h-8 w-8 text-black" />
                </Button>
              </SheetTrigger>
              <SheetContent side="left">
                <SheetHeader className="p-6">
                  <SheetTitle>
                    <div className="flex items-start">
                      <Link href="/" className="flex items-center gap-2">
                        <Icons.logo className="w-8 h-8" />
                        <span className="text-lg font-medium">Clearmind</span>
                      </Link>
                    </div>
                  </SheetTitle>
                </SheetHeader>
                <div className="flex flex-col">
                  <DialogClose asChild>
                    <Link href="#features" className="py-3 cursor-pointer px-8 ">
                      Features
                    </Link>
                  </DialogClose>
                  <DialogClose asChild>
                    <Link href="#about-us" className="py-3 cursor-pointer px-8 ">
                      About Us
                    </Link>
                  </DialogClose>
                  <DialogClose asChild>
                    <Link href="#compliant" className="py-3 cursor-pointer px-8 ">
                      Compliant Board
                    </Link>
                  </DialogClose>
                  
                </div>
              </SheetContent>
            </Dialog>
          </div>

            <Link href="/" className="flex items-center gap-2">
              <Icons.logo className=" w-4 h-6 md:w-8  md:h-8" />
              <div className="flex flex-col">
              <div className="text-lg font-medium leading-[22px]">Clearmind</div>
              <div className="text-[8px] md:text-[10px] font-normal text-muted-foreground">System is the answer</div>

              </div>
            </Link>
            
          </div>

          

          <nav className="hidden lg:block absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
            <ul className="flex items-center justify-center gap-8">
              <Link href="#features" className="  font-semibold  hover:text-foreground/80 text-sm">
                Features
              </Link>
              <Link href="#about-us" className="  font-semibold  hover:text-foreground/80 text-sm">
                About Us
              </Link>
              <Link href="#compliant" className="  font-semibold  hover:text-foreground/80 text-sm">
                Compliant Board
              </Link>
              
            </ul>
          </nav>

          <div className="flex items-center gap-4">
            
            <Link
              href="https://cal.com/iamclearmind"
              target="_blank"
              className={buttonVariants({
                size: "sm",
                className: "flex ",
              })}
            >
              Connect with Us
            </Link>
          </div>
        </div>
      </Container>
    </header>
  );
};

export default Navbar;
