import GrievanceTable from "@/components/global/grievance-table";
import Icons from "@/components/global/icons";
import {
  Heart,
  LocateIcon,
  MailIcon,
  MapPinIcon,
  PhoneCallIcon,
} from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { FaTwitter } from "react-icons/fa";
import { FaLinkedin } from "react-icons/fa";

const Footer = () => {
  return (
    <footer className="flex bg-zinc-900 flex-col relative items-center justify-center border-t border-border pt-10 pb-8 px-4 lg:px-8 w-full  lg:pt-32">
      <div className="bg-transparent mb-8">
        <GrievanceTable />
      </div>

      <div className="grid gap-8 xl:grid-cols-2 xl:gap-8 w-full max-w-6xl mx-auto">
        <div className="flex flex-col items-start justify-start max-w-full md:max-w-[400px]">
          <div className="flex items-center justify-center gap-2">
            <Icons.logo_dark className="w-7 h-7" />
            <div className="flex flex-col">
              <div className="text-lg font-medium leading-[22px] text-white">
                Clearmind
              </div>
              <div className="text-[12px] font-normal text-muted-foreground">
                System is the answer
              </div>
            </div>
          </div>
          <p className="text-muted-foreground mt-4 text-xs text-start">
            Punam Kucheria, a SEBI-registered Research Analyst and founder of
            Clearmind ., boasts 25+ years in stock markets. A Symbiosis alumnus,
            his investment philosophy prioritizes systematic, bias-free
            long-term approaches. Mr. Kucheria's zeal is in crafting advanced,
            automated, high-yield investing models for reliable passive income
            and wealth accumulation. Driven by the belief in universal financial
            freedom, he guides individuals and businesses toward prosperity.
            Clearmind Consultancy, synonymous with financial success, embodies
            Mr. Kucheria's commitment to empowering others economically. Explore
            our latest report here.
          </p>
        </div>

        <div className="grid-cols-1 place-items-start gap-8 grid mt-6 xl:col-span-1 xl:mt-0">
          <div className="grid grid-cols-2 md:grid-cols-4 md:gap-x-1 md:gap-y-8">
            <div className="">
              <h3 className="text-base font-medium text-white">Explore</h3>
              <ul className="mt-4 text-sm text-muted-foreground">
                <li className="">
                  <Link
                    href="#features"
                    className="hover:text-white/80  transition-all duration-300"
                  >
                    Features
                  </Link>
                </li>
                <li className="mt-2">
                  <Link
                    href="#about-us"
                    className="hover:text-white/80  transition-all duration-300"
                  >
                    About Us
                  </Link>
                </li>

                {/* <li className="mt-2">
                  <Link
                    href="#testimonials"
                    className="hover:text-white/80  transition-all duration-300"
                  >
                    Testimonials
                  </Link>
                </li> */}
              </ul>
            </div>
            <div className="">
              <h3 className="text-base font-medium text-white">Resources</h3>
              <ul className="mt-4 text-sm text-muted-foreground">
                <li className="mt-2">
                  <Link
                    href="https://iamclearmind.smallcase.com/#disclosures"
                    className="hover:text-white/80  transition-all duration-300"
                  >
                    Disclosures
                  </Link>
                </li>
                <li className="mt-2">
                  <Link
                    href="https://www.smallcase.com/meta/terms-and-conditions"
                    className="hover:text-white/80  transition-all duration-300"
                  >
                    Terms & Conditions
                  </Link>
                </li>
                <li className="mt-2">
                  <Link
                    href="https://iamclearmind.smallcase.com/#privacy"
                    className="hover:text-white/80  transition-all duration-300"
                  >
                    Privacy Policy
                  </Link>
                </li>
                <li className="mt-2">
                  <Link
                    href="/assets/investor-charter.pdf"
                    download="Investor_Charter"
                    target="_blank"
                    className="hover:text-white/80 transition-all duration-300"
                  >
                    Investor Charter
                  </Link>
                </li>
              </ul>
            </div>
            <div className="mt-10 md:mt-0 md:ml-6 flex flex-col col-span-2">
              <h3 className="text-base font-medium text-white">Contact Us</h3>
              <ul className="mt-4 text-sm text-muted-foreground">
                <li className="">
                  <Link
                    href="mailto:admin@clearmind.com"
                    className="flex items-center"
                  >
                    <MailIcon className=" w-4 h-4 text-muted-foreground" />
                    <div className="ml-3 hover:text-white/80  transition-all duration-300">
                      admin@clearmind.com
                    </div>
                  </Link>
                </li>
                <li className="flex items-center mt-2">
                  <PhoneCallIcon className=" w-4 h-4 text-muted-foreground" />
                  <div className="ml-3 hover:text-white/80  transition-all duration-300">
                    +919372810916
                  </div>
                </li>
                <li className="flex  items-start mt-2">
                  <MapPinIcon className=" w-10 md:w-12 h-5 text-muted-foreground" />
                  <div className=" ml-3 flex fle-wrap hover:text-white/80  transition-all duration-300">
                    106, 1st Floor, Petrol Pump, Jewel Square mall, next to
                    North Main Road, Koregaon Park, Pune, Maharashtra 411001,
                    India
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="mt-8 border-t-[1px] border-zinc-800 pt-4 md:pt-8 md:flex md:items-center md:justify-between w-full max-w-6xl mx-auto">
        <p className="text-sm text-muted-foreground mt-8 md:mt-0">
          &copy; {new Date().getFullYear()} Clearmind . All rights reserved.
        </p>

        <div className="flex items-center space-x-3 md:space-x-6 mt-3 md:mt-0">
          <Link
            href="https://stocktwits.com/Punam_Kucheria"
            target="_blank"
            className="flex items-center text-muted-foreground hover:text-white/80 transition-all duration-300"
          >
            <Image
              src="/assets/stockwits.png"
              className="mr-2 w-5 h-5 rounded-md"
              alt="StockTwits"
              width={20}
              height={20}
            />
            Stocktwits
          </Link>
          <Link
            href="https://x.com/iam_clearmind"
            target="_blank"
            className=" flex items-center text-muted-foreground hover:text-white/80  transition-all duration-300"
          >
            <FaTwitter className="mr-2 w-4 h-4 text-[#1da1f2]" />
            Twiter
          </Link>

          <Link
            href="https://www.linkedin.com/company/iamclearmind"
            target="_blank"
            className=" flex items-center text-muted-foreground hover:text-white/80  transition-all duration-300"
          >
            <FaLinkedin className="mr-2 w-4 h-4 text-[#0077B5]" />
            LinkedIn
          </Link>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
