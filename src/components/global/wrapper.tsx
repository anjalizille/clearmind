import React from 'react'
import { cn } from "@/lib/utils";

interface Props {
    id?: string;
    className?: string;
    children: React.ReactNode;
}

const Wrapper = ({ children, id, className }: Props) => {
    return (
        <div id={id} className={cn(
            "h-full mx-auto w-full  px-4 md:px-0",
            className
        )}>
            {children}
        </div>
    )
};

export default Wrapper
