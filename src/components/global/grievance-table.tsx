"use client";

import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";

export default function GrievanceTable() {
  return (
    <Card className="border-none w-[340px] md:w-full md:max-w-6xl mx-auto overflow-hidden  bg-transparent ">
      <CardContent className="p-0 ">
        <Table className="">
          <TableHeader>
            <TableRow className="bg-zinc-700 border-b-[1px] border-zinc-600 hover:bg-zinc-700 text-white/80 rounded-t-md">
              <TableHead className="py-3 text-[12px] text-white/80 border-r-[1px] border-zinc-600 rounded-tl-md">
                S.No
              </TableHead>
              <TableHead className="py-3 text-[12px] min-w-[180px] md:w-auto text-white/80 border-r-[1px] border-zinc-600">
                Details of designation
              </TableHead>
              <TableHead className="py-3 text-[12px] min-w-[160px] md:w-auto text-white/80 border-r-[1px] border-zinc-600">
                Contact Person Name
              </TableHead>
              <TableHead className="py-3 text-[12px] min-w-[150px] text-white/80 border-r-[1px] border-zinc-600">
                Address (Physical Address Location)
              </TableHead>
              <TableHead className="py-3 text-[12px] min-w-[140px] text-white/80 border-r-[1px] border-zinc-600">
                Contact No.
              </TableHead>
              <TableHead className="py-3 text-[12px] text-white/80 border-r-[1px] border-zinc-600">
                Email ID
              </TableHead>
              <TableHead className="py-3 text-[12px] text-white/80 border-r-[1px] border-zinc-600 rounded-tr-md">
                Working hours when compliant can call
              </TableHead>
            </TableRow>
          </TableHeader>
          <TableBody className="bg-white">
            <TableRow>
              <TableCell className="border-r-[1px] border-zinc-200">1</TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200 ">
                Customer Care
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Mr. Punam Kucheria
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                15 Goldfield Enclave 397 South Main Road Koregaon Park
                Pune-411001
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                +91 9372810196
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                pkucheria@gmail.com
              </TableCell>
              <TableCell className="text-left text-[13px]">
                10:00 AM - 5:00 PM
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell className="border-r-[1px] border-zinc-200">2</TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Head of Customer Care
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Mr. Punam Kucheria
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                15 Goldfield Enclave 397 South Main Road Koregaon Park
                Pune-411001
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                +91 9372810196
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                pkucheria@gmail.com
              </TableCell>

              <TableCell className="text-left text-[13px]">
                10:00 AM - 5:00 PM
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell className="border-r-[1px] border-zinc-200">3</TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Compliance Officer
              </TableCell>

              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Mr. Punam Kucheria
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                15 Goldfield Enclave 397 South Main Road Koregaon Park
                Pune-411001
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                +91 9372810196
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                pkucheria@gmail.com
              </TableCell>

              <TableCell className="text-left text-[13px]">
                10:00 AM - 5:00 PM
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell className="border-r-[1px] border-zinc-200">4</TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                CEO
              </TableCell>

              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Mr. Punam Kucheria
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                15 Goldfield Enclave 397 South Main Road Koregaon Park
                Pune-411001
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                +91 9372810196
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                pkucheria@gmail.com
              </TableCell>

              <TableCell className="text-left text-[13px]">
                10:00 AM - 5:00 PM
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell className="border-r-[1px] border-zinc-200">5</TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Principal Officer
              </TableCell>

              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                Mr. Punam Kucheria
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                15 Goldfield Enclave 397 South Main Road Koregaon Park
                Pune-411001
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                +91 9372810196
              </TableCell>
              <TableCell className="text-left text-[13px] border-r-[1px] border-zinc-200">
                pkucheria@gmail.com
              </TableCell>

              <TableCell className="text-left text-[13px]">
                10:00 AM - 5:00 PM
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
}
