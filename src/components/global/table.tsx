"use client"

import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"

export default function ComplaintTable() {

  
  return (
    <Card className="mt-1 border-none w-[340px] md:w-full md:max-w-6xl mx-auto overflow-hidden  bg-transparent">
      <CardContent className="p-0 ">
        <Table className="">
          <TableHeader>
            <TableRow className="bg-zinc-700 border-b-[1px] border-zinc-600 hover:bg-zinc-700 text-white/80 rounded-t-md">
              <TableHead className="py-3 text-white/80 border-r-[1px] border-zinc-600 rounded-tl-md">S.No</TableHead>
              <TableHead className="py-3 min-w-[180px] md:w-auto text-white/80 border-r-[1px] border-zinc-600">Received From</TableHead>
              <TableHead className="py-3 min-w-[180px] md:w-auto text-white/80 border-r-[1px] border-zinc-600">Pending at the end of last month</TableHead>
              <TableHead className="py-3 text-white/80 border-r-[1px] border-zinc-600">Received</TableHead>
              <TableHead className="py-3 text-white/80 border-r-[1px] border-zinc-600">Resolved</TableHead>
              <TableHead className="py-3 text-white/80 border-r-[1px] border-zinc-600">Total Pending</TableHead>
              <TableHead className="py-3 min-w-[180px] md:w-auto text-white/80 border-r-[1px] border-zinc-600">Pending Complaints &gt; 3 months</TableHead>
              <TableHead className="py-3 min-w-[180px] md:w-auto text-white/80 rounded-tr-md ">Average Resolution Time</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody className="bg-white">
            <TableRow>
              <TableCell>1</TableCell>
              <TableCell className="text-left border-r-[1px] border-zinc-200">Support</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right">0</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>2</TableCell>
              <TableCell className="text-left border-r-[1px] border-zinc-200">SEBI Scores</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right">0</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>3</TableCell>
              <TableCell className="text-left border-r-[1px] border-zinc-200">Other Sources</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right">0</TableCell>
            </TableRow>
            <TableRow className="font-semibold">
              <TableCell className="sr-only">0</TableCell>
              <TableCell className="text-left border-r-[1px] border-zinc-200">Grand Total</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right border-r-[1px] border-zinc-200">0</TableCell>
              <TableCell className="text-right ">0</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  )
}
