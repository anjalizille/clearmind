"use client";

import { Container, Icons, Wrapper } from "@/components";
import { BorderBeam } from "@/components/ui/border-beam";
import { Button } from "@/components/ui/button";
import { HiBadgeCheck, HiShieldCheck } from "react-icons/hi";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { LampContainer } from "@/components/ui/lamp";
import Marquee from "@/components/ui/marquee";
import SectionBadge from "@/components/ui/section-badge";
import { features, perks, pricingCards, reviews } from "@/constants";
import { cn } from "@/lib/utils";
import {
  ArrowRight,
  BadgeCheckIcon,
  ChevronRight,
  ShieldCheckIcon,
  UserIcon,
  Zap,
} from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import ComplaintTable from "@/components/global/table";

const HomePage = () => {
  const firstRow = reviews.slice(0, reviews.length / 2);
  const secondRow = reviews.slice(reviews.length / 2);

  const getFormattedDate = () => {
    const today = new Date();

    const day = today.getDate();
    const month = today.toLocaleString("default", { month: "short" });
    const year = today.getFullYear();

    const getOrdinalSuffix = (n: any) => {
      const suffixes = ["th", "st", "nd", "rd"];
      const v = n % 100;
      return suffixes[(v - 20) % 10] || suffixes[v] || suffixes[0];
    };

    const dayWithSuffix = `${day}${getOrdinalSuffix(day)}`;

    return `${dayWithSuffix} ${month} ${year}`;
  };

  return (
    <section className=" w-full relative flex items-center justify-center flex-col px-0 md:px-0 py-8">
      <Wrapper>
        <div className="absolute inset-0 opacity-[0.1] dark:bg-[linear-gradient(to_right,#e2e8f0_1px,transparent_1px),linear-gradient(to_bottom,#e2e8f0_1px,transparent_1px)] bg-[linear-gradient(to_right,#161616_1px,transparent_1px),linear-gradient(to_bottom,#161616_1px,transparent_1px)] bg-[size:4rem_4rem] [mask-image:radial-gradient(ellipse_60%_50%_at_50%_0%,#000_70%,transparent_110%)] -z-10" />

        <Container>
          <div className="flex flex-col items-center justify-center py-16 h-full">
            <button className="group relative grid overflow-hidden rounded-full px-4 py-1 shadow-[0_1000px_0_0_hsl(0_0%_20%)_inset] transition-colors duration-200">
              <span>
                <span className="spark mask-gradient absolute inset-0 h-[100%] w-[100%] animate-flip overflow-hidden rounded-full [mask:linear-gradient(white,_transparent_50%)] before:absolute before:aspect-square before:w-[200%] before:rotate-[-90deg] before:animate-rotate before:bg-[conic-gradient(from_0deg,transparent_0_340deg,white_360deg)] before:content-[''] before:[inset:0_auto_auto_50%] before:[translate:-50%_-15%]" />
              </span>
              <span className="backdrop absolute inset-[1px] rounded-full bg-neutral-950 transition-colors duration-200 group-hover:bg-neutral-900" />
              <span className="h-full w-full blur-md absolute bottom-0 inset-x-0 bg-gradient-to-tr from-primary/40"></span>
              <span className="z-10 py-0.5 text-sm text-neutral-100 flex items-center justify-center gap-1.5">
                <Image
                  src="/icons/sparkles-dark.svg"
                  alt="✨"
                  width={24}
                  height={24}
                  className="w-4 h-4"
                />
                Introducing Clearmind
                <ChevronRight className="w-4 h-4" />
              </span>
            </button>

            <div className="flex flex-col items-center mt-8 max-w-3xl w-11/12 md:w-full">
              <h1 className="text-4xl md:text-6xl lg:text-5xl md:!leading-snug font-semibold text-center bg-clip-text bg-gradient-to-b from-gray-50 to-gray-50 text-black">
                Achieve your investment goals with Clearmind Research Desk
              </h1>
              <p className="text-base md:text-lg text-foreground/80 mt-6 text-center">
                Get access to investment portfolios created by research experts
              </p>
              <Link
                href="https://cal.com/iamclearmind/talk"
                target="_blank"
                className="mt-8"
              >
                <Button
                  size="sm"
                  className="rounded-full hidden lg:flex border border-foreground/20"
                >
                  Connect Now
                  <ArrowRight className="w-4 h-4 ml-1" />
                </Button>
              </Link>
            </div>
          </div>
        </Container>
      </Wrapper>

      <Wrapper
        id="features"
        className="flex flex-col items-center justify-center py-12 relative bg-zinc-900"
      >
        <Container>
          <div className="max-w-md mx-auto text-start md:text-center">
            <SectionBadge title="Features" />
            <h2 className="text-3xl lg:text-4xl font-semibold mt-6 text-white">
              Discover our powerful features
            </h2>
            <p className="text-muted-foreground mt-6">
              Our unique features empower you to navigate the markets with
              precision and confidence.
            </p>
          </div>
        </Container>

        <Container>
          <div className="flex flex-col items-center justify-center py-10 md:py-20 md:px-10 w-full max-w-6xl mx-auto">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 w-full gap-8">
              {features.map((feature, idx) => (
                <div key={idx} className=" p-1 rounded-lg">
                  <div
                    key={feature.title}
                    className="flex flex-col items-start lg:items-start "
                  >
                    <div className="flex items-center justify-center">
                      <feature.icon className="w-8 h-8" />
                    </div>
                    <h3 className="text-lg font-medium mt-4 text-white">
                      {feature.title}
                    </h3>
                    <p className="text-muted-foreground text-sm mt-2 text-start lg:text-start rounded-md">
                      {feature.info}
                    </p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </Container>
      </Wrapper>

      <Wrapper
        id="about-us"
        className="flex flex-col items-center justify-center py-12 relative"
      >
        <Container>
          <div className="max-w-md mx-auto text-start md:text-center">
            <SectionBadge title="About Us" />
            <h2 className="text-3xl lg:text-4xl font-semibold mt-6 text-black">
              Explore Our Vision
            </h2>
          </div>
        </Container>
        <Container className="flex flex-col space-y-6 py-6 md:py-16 md:space-y-0 md:flex-row  justify-center">
          <div className="flex flex-col">
            <div className=" mb-2 md:mb-0">
              <Image
                src="/assets/about-us.png" // Replace with your image path
                alt="About Us"
                width={180} // Adjust width as needed
                height={150} // Adjust height as needed
                className="w-[340px] h-auto rounded-lg shadow-lg"
              />
            </div>
            <div className="flex items-center justify-between w-full py-2 max-w-[340px]">
              <div className="flex items-start">
                <ShieldCheckIcon className=" h-7 w-7 fill-blue-500 stroke-white" />
                <div>
                  <div className="text-muted-foreground text-[10px] mt-1">
                    Years of experience
                  </div>
                  <div className="text-black text-[12px] font-semibold">
                    20+ Years
                  </div>
                </div>
              </div>
              <div className="flex items-start">
                <BadgeCheckIcon className=" h-7 w-7 fill-green-500 stroke-white" />
                <div>
                  <div className="text-muted-foreground text-[10px] mt-1">
                    SEBI Registered
                  </div>
                  <div className="text-black text-[12px] font-semibold">
                    INH000010098
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="md:w-[700px] md:pl-16">
            <h2 className="text-3xl font-semibold text-black">
              Mr. Punam Kucheria
            </h2>
            <p className="mt-4 text-base text-gray-700 leading-[24px] md:leading-[28px]">
              Mr. Punam Kucheria is a SEBI registered Research Analyst and the
              founder of Clearmind , a name synonymous with financial success.
              He is also a Symbiosis alumnus with over 25 years of experience in
              Stock markets. Mr. Kucheria's investment philosophy revolves
              around the power of systematic investments for the long term,
              devoid of any human bias. His passion lies in developing
              cutting-edge, fully automated high-yielding investing models,
              designed to be a reliable source of passive income and wealth
              accumulation. Driven by the belief that financial freedom should
              be accessible to all, he has dedicated his career to guiding
              individuals and businesses on their path to financial prosperity.
            </p>
          </div>
        </Container>
      </Wrapper>

      {/* <Wrapper id="testimonials" className="flex flex-col items-center justify-center py-12 relative bg-zinc-900">
        <div className="hidden md:block absolute -top-1/4 -left-1/3 w-72 h-72 bg-indigo-500 rounded-full blur-[10rem] -z-10"></div>
        <Container>
          <div className="max-w-md mx-auto text-start md:text-center">
            <SectionBadge title="Our Customers" />
            <h2 className="text-3xl lg:text-4xl font-semibold mt-6 text-white">
              What people are saying
            </h2>
            <p className="text-muted-foreground mt-6">
              See how Astra empowers businesses of all sizes. Here&apos;s what
              real people are saying on Twitter
            </p>
          </div>
        </Container>
        <Container>
          <div className="py-10 md:py-20 w-full">
            <div className="relative flex h-full w-full flex-col items-center justify-center overflow-hidden py-10">
              <Marquee pauseOnHover className="[--duration:20s] select-none">
                {firstRow.map((review, i) => (
                  <figure
                    key={i}
                    className={cn(
                      "relative w-[300px] cursor-pointer overflow-hidden rounded-xl border px-6 py-8",
                      "border-zinc-50/[.1] bg-zinc-800 over:bg-zinc-50/[.15]"
                    )}
                  >
                    <div className="flex flex-row items-center  space-x-3">
                      <div className="w-8 h-8 flex items-center justify-center rounded-full ring-2 ring-white/40 ">
                        <UserIcon className="w-6 h-6 text-white" />
                      </div>
                      <div className="flex flex-col ">
                        <figcaption className="font-medium text-white text-[14px]">
                          {review.name}
                        </figcaption>
                        <p className="text-xs font-medium text-zinc-400">
                          {review.username}
                        </p>
                      </div>
                    </div>
                    <blockquote className="mt-6 text-sm text-zinc-200">
                      {review.body}
                    </blockquote>
                  </figure>
                ))}
              </Marquee>
              <Marquee
                reverse
                pauseOnHover
                className="[--duration:20s] select-none"
              >
                {secondRow.map((review, idx) => (
                  <figure
                    key={idx}
                    className={cn(
                      "relative w-[300px] cursor-pointer overflow-hidden rounded-xl border px-6 py-8",
                      "border-zinc-50/[.1] bg-zinc-800 over:bg-zinc-50/[.15]"
                    )}
                  >
                    <div className="flex flex-row items-center  space-x-3">
                      <div className="w-8 h-8 flex items-center justify-center rounded-full ring-2 ring-white/40 ">
                        <UserIcon className="w-6 h-6 text-white" />
                      </div>
                      <div className="flex flex-col ">
                        <figcaption className="font-medium text-white text-[14px]">
                          {review.name}
                        </figcaption>
                        <p className="text-xs font-medium text-zinc-400">
                          {review.username}
                        </p>
                      </div>
                    </div>
                    <blockquote className="mt-6 text-sm text-zinc-200">
                      {review.body}
                    </blockquote>
                  </figure>
                ))}
              </Marquee>
              <div className="pointer-events-none absolute inset-y-0 left-0 w-1/3 bg-gradient-to-r from-zinc-900"></div>
              <div className="pointer-events-none absolute inset-y-0 right-0 w-1/3 bg-gradient-to-l from-zinc-900"></div>
            </div>
          </div>
        </Container>
      </Wrapper> */}

      <Wrapper
        id="compliant"
        className="flex flex-col  items-center justify-center py-12 relative bg-zinc-900"
      >
        <Container>
          <div className="max-w-md mx-auto text-start md:text-center">
            <SectionBadge title="Compliant Board" />
            <h2 className="text-3xl lg:text-4xl font-semibold mt-6 text-white ">
              Compliant Board
            </h2>
            <p className="text-muted-foreground mt-6">
              Stay Informed with the Latest Compliance Insights
            </p>
          </div>
        </Container>
        <Container>
          <div className="mt-8 px-2 flex text-sm justify-end font-medium text-muted-foreground w-[340px] md:w-full md:max-w-6xl mx-auto">
            Last updated on :
            <span className="ml-1 text-white ">{getFormattedDate()}</span>
          </div>
          <ComplaintTable />
        </Container>
      </Wrapper>

      <Wrapper className="flex  items-center justify-center py-12 relative ">
        <Container>
          <div className="flex flex-col items-center justify-center relative w-full text-center">
            <h2 className="text-3xl lg:text-5xl xl:text-6xl lg:!leading-snug font-semibold mt-8 t">
              Invest Better with <br /> Clearmind
            </h2>
            <p className="text-muted-foreground mt-6 max-w-md mx-auto">
              Backed by time tested principles and solid research.
            </p>
            <Button variant="white" className="mt-6  font-semibold" asChild>
              <Link href="https://cal.com/iamclearmind" target="_blank">
                Connect Now
                <ArrowRight className="w-4 h-4 ml-2" />
              </Link>
            </Button>
          </div>
        </Container>
      </Wrapper>
    </section>
  );
};

export default HomePage;
